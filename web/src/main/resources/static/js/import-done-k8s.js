
window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)',
    edirexPurple: '#914b98',
    edirexBlue: '#3b8fa1'
};


var k8sApiInstanceUrl = null;

var serverUrl = "https://" + location.hostname + "/data";
//var serverUrl = "http://localhost:8080/data";

var instanceUser = getParameterValue('user');
var instanceId = getParameterValue('id');
var instanceHref = getParameterValue('href');

var exportCancelUrl = serverUrl + "/export-cancel?user=" + instanceUser + "&id=" + instanceId;
var exportStatusUrl = serverUrl + "/export-status?user=" + instanceUser + "&id=" + instanceId;

var btnCbio = document.getElementById("btn-cbio");
var progressBar = document.getElementById("BarProgress");
var xhr = new XMLHttpRequest();
var progress;

var inProgress = true;
// Disable the button on initial page load
btnCbio.disabled = true;

var attemptCount = 30;
var attemptsRemaining = attemptCount;


function endExport() {
    var xhrEnd = new XMLHttpRequest();

    xhrEnd.open('GET', exportCancelUrl);
    xhrEnd.send();

    setTimeout(function () {
        window.location.replace("/search");
    }, 1);
}

function runExport() {
    var xhrGetK8sApi = new XMLHttpRequest();
    xhrGetK8sApi.onreadystatechange = processRequest;
    xhrGetK8sApi.open('GET', '/data/get-k8s-api-url', false);
    xhrGetK8sApi.send();
    var item = JSON.parse(xhrGetK8sApi.responseText);
    k8sApiInstanceUrl = item.url;

    var progressBar = document.getElementById("progress_bar");
    var btnExport = document.getElementById("btn-export");
    var btnCbio = document.getElementById("btn-cbio");

    progressBar.style.display = "unset";
    btnExport.style.display = "none";
    btnCbio.style.display = "unset";

    request();
}

function processRequest(e) {
    if (xhr.readyState === 4 && xhr.status === 202) {
        if(inProgress) {
            progress = progressBar.getAttribute('aria-valuenow');
            var width = 'width: ';

            if (btnCbio.disabled === true) {
                if (progress !== 99) {
                    progress = +progress + 1;
                    width += progress;
                    width += "%";

                    progressBar.setAttribute('aria-valuenow', progress);
                    progressBar.setAttribute('style', width);
                }
            }

            var continueProgress = true;
            switch (xhr.responseText) {
                case "active":
                    continueProgress = false;
                    btnCbio.setAttribute('href', k8sApiInstanceUrl + '/' + instanceHref);
                    if (btnCbio.disabled === true) {
                        btnCbio.disabled = !btnCbio.disabled;
                        progressBar.setAttribute('aria-valuenow', '100');
                        progressBar.setAttribute('style', 'width: 100%');
                        btnCbio.innerHTML = "cBio Portal";
                        btnCbio.style.backgroundColor = "green";

                        // hide progress bar
                        document.getElementById("progress_bar").style.display = "none";
                    }
                    break;
                case "creating":
                    btnCbio.innerHTML = "Creating instance...";
                    break;
                case "importing":
                    btnCbio.innerHTML = "Importing data...";
                    break;
            }

            /*
            if (continueProgress) {
                inProgress = false;
                setTimeout(request, 4000);
            }*/

            inProgress = false;
        }

        attemptsRemaining = attemptCount;
    }
    else if (xhr.readyState === 4 && xhr.status !== 202) {
        if (attemptsRemaining > 0) {
            attemptsRemaining--;
        } else {
            alert("cBioPortal is currently unavailable, please try later.");
            window.location.href = "/search";
        }
    }
}

function request() {
    inProgress = true;

    xhr.onreadystatechange = processRequest;

    xhr.open('GET',exportStatusUrl, true);
    xhr.send();
    xhr.addEventListener("readystatechange", processRequest, false);

    setTimeout(request, 4000);
}

function getParameterValue(name)
{
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null ) return "";
    else return results[1];
}
