var appMoudule = angular.module('appController', []);
var getAll = '/data/instance-status-all';

function getUser() {
    var user = document.getElementById('user');
    var returns = user.innerText;
    return returns;
}

appMoudule.filter("convertTime", function () {
    return function (time) {
        return (new Date).clearTime()
            .addSeconds(time)
            .toString('HH:mm:ss');
    };
});

appMoudule.controller("AppController", function ($scope, $http, $interval) {
    var ctr = this;
    ctr.k8s = null;

    $http.get('/data/get-k8s-api-url')
        .then(function successCallback(response) {
                ctr.k8s = response.data.url;
            }, function errorCallback(response) {
                ctr.k8s = 'https://cbiood.edirex.ics.muni.cz/';
                console.error('Get k8s api failed. Set to default value: ', response);
            }
        );

    ctr.extendLifetime = "";
    ctr.state = 'loading';
    ctr.instances = [];

    var attemptCount = 5;
    var attemptsRemaining = attemptCount;

    var removingInstances = [];

    ctr.updateCountdown = function () {
        for (var i = 0, len = ctr.instances.length; i < len; i++) {
            ctr.instances[i].secondsToExpire--;
        }
    };

    ctr.getInstances = function (userId) {
        console.log("retrieving instances for " + userId);
        $http.get(getAll,{
            params : {user :userId}
        }).then(function successCallback(response) {
                console.log('Retrieved this data ', response.data);
                var instancesTemp = response.data;

                // mark removing instance
                // there is some delay because k8s api - it return deleted instances some seconds after delete
                // +
                // mark instances with 'removing' status after countdown overlap
                var toRemove = [];
                for (var i = 0, len = instancesTemp.length; i < len; i++) {
                    removingInstances.forEach(function (instance) {
                        if (instance === instancesTemp[i].id) {
                            instancesTemp[i].deleteProgress = 'true';
                        }
                    });

                    // when instance has more than 23:30 hours (84600 secs) TTL > mark for remove from list because k8s remove overlap
                    if (instancesTemp[i].secondsToExpire > 84600) {
                        toRemove.push(instancesTemp[i].id);
                    }
                }

                // create final array with filtered instances
                ctr.instances = [];
                instancesTemp.forEach(function (instance) {
                    if (!toRemove.includes(instance.id)) {
                        ctr.instances.push(instance);
                    }
                });

                attemptsRemaining = attemptCount;

                if (ctr.instances.length === 0) {
                    ctr.state = 'empty';
                } else {
                    ctr.state = 'nonempty';
                }

                ctr.updateCountdown();
            }, function errorCallback(response) {
                console.error('Services not available. Response: ', response);
                ctr.instances =  [];
                attemptsRemaining--;

                if (attemptsRemaining > 0) {
                    ctr.state = 'loading';
                } else {
                    ctr.state = 'unavailable';
                }
            }
        );
    };

    $scope.openLink = function(link) {
        window.open(link, '_blank');
    };

    $scope.extendInstance = function(instance) {
        var extendSeconds = 60; // add 1 minute because imprecision
        if (ctr.extendLifetime.search(':') === -1) {
            extendSeconds += ctr.extendLifetime * 60
        } else {
            var time = ctr.extendLifetime.split(':');
            extendSeconds += time[0] * 3600;
            extendSeconds += time[1] * 60;
        }

        // extend instance up to 23:00
        // 82800 - 23:00 in seconds
        var limit = 82800;
        if (instance.secondsToExpire + extendSeconds > limit) {
            extendSeconds = limit - instance.secondsToExpire;
        }

        $http.get('/data/instance-extend?user=' + instance.user.userId + '&id=' + instance.id + '&extend=' + extendSeconds)
            .then(function successCallback(response) {
                }, function errorCallback(response) {
                    console.error('Extend instance lifetime failed. Response: ', response);
                }
            );

        ctr.getInstances(ctr.user);
    };

    $scope.deleteInstance = function(instance) {
        instance.deleteProgress = 'true';
        removingInstances.push(instance.id);

        $http.get('/data/instance-delete?user=' + instance.user.userId + '&id=' + instance.id)
            .then(function successCallback(response) {
                }, function errorCallback(response) {
                    console.error('Delete instance failed. Response: ', response);
                }
            );

        ctr.getInstances(ctr.user);
    };

    ctr.user = getUser();

    ctr.getInstances(ctr.user);
    $interval(function () {
        ctr.getInstances(ctr.user);
    }, 5000);

    ctr.updateCountdown();
    $interval(function () {
        ctr.updateCountdown();
    }, 1000);

    console.log(ctr.user + ' logging ' );
});
