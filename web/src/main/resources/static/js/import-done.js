
window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)',
    edirexPurple: '#914b98',
    edirexBlue: '#3b8fa1'
};

var statsShowed = false;

var button = document.getElementById("btn-cbio");
var xhr;
var xhrStats;
var xhrStats2;

//var serverUrl = "http://localhost:8080/data/";
var serverUrl = "https://dataportal.europdx.eu/data/";
//var serverUrl = "http://playground.edirex.ics.muni.cz/data/home/";

var bar = document.getElementById("BarProgress");
var progress;

var port = getParameterValue( 'port');
var href = button.getAttribute('href');
var can = true;
// Disable the button on initial page load
button.disabled = true;

request();
// tu treba zavolat funkciu ktora bude zistovat cez dataportal ci uz su IDecka spracovane ak hej tak ich musim dat do HTML

function endExport() {
    var xhrEnd = new XMLHttpRequest();
    xhrEnd.onreadystatechange = processRequest;
    xhrEnd.open('GET', serverUrl + "/cbioend?id=" + port);
    xhrEnd.send();

    setTimeout(function () {
        window.location.replace(serverUrl);
    }, 1000);
}

function processRequest(e) {

    // alert(xhr.readyState );
    if (xhr.readyState == 4 && xhr.status == 202) {
        // time to partay!!!
        if(can){
            progress = bar.getAttribute('aria-valuenow');
            // alert(progress);
            var width = 'width: ';

            if(button.disabled == true){
                if (progress != 95){
                    progress = +progress +5;
                    width = width + progress;
                    // alert("1 : " + width);
                    width = width +  "%";
                    // alert("2 : " +width);

                    bar.setAttribute('aria-valuenow',progress);
                    bar.setAttribute('style',width);
                    // alert("3 : " + bar.getAttribute('style'));
                }
            }


            if(xhr.responseText == "Finish"){
                button.setAttribute('href', href);
                if(button.disabled == true){
                    button.disabled = !button.disabled;
                    bar.setAttribute('aria-valuenow','100');
                    bar.setAttribute('style','width: 100%');
                    button.innerHTML = "CBio Portal";

                    // hide progress bar
                    document.getElementById("progress").style.display = "none";

                    // alert("4 : " + bar.getAttribute('aria-valuenow'));
                    // alert("5 : " + bar.getAttribute('style'));
                }
            }else if(xhr.responseText == "Fail") {
                alert("cBioPortal is currently unavailable, please try later.");
                window.location.href = "https://dataportal.europdx.eu/data/search";
            }else{
                // alert("volam");
                can = false;
                setTimeout(request,15000);
            }

            // get and print stats
            if (!statsShowed) {
                xhr.open('GET', serverUrl + "/stats?id=" + port);
                xhr.send();
                xhr.addEventListener("readystatechange", showStats, false);
            }
        }
    }
}

function printUnknownIdsRatio() {
    document.getElementById("unknown-ids-header").innerText = xhrStats.responseText;
}

function printUnknownIdsList() {
    // show ids
    document.getElementById("unknown-ids").style.display = "contents";
    document.getElementById("unknown-ids-list").innerText = xhrStats2.responseText;
}

/**
 * Print and show stats
 */
function showStats() {
    var statsLine = xhr.responseText;

    if (statsLine === "" || statsLine === "IMPORT")
        return;

    // if statsLine != "" then printIdsUnknown()
    xhrStats = new XMLHttpRequest();
    xhrStats.onreadystatechange = processRequest;
    xhrStats.open('GET', serverUrl + "/unknown-ids-ratio?id=" + port);
    xhrStats.send();
    xhrStats.addEventListener("readystatechange", printUnknownIdsRatio, false);

    xhrStats2 = new XMLHttpRequest();
    xhrStats2.onreadystatechange = processRequest;
    xhrStats2.open('GET', serverUrl + "/unknown-ids-list?id=" + port);
    xhrStats2.send();
    xhrStats2.addEventListener("readystatechange", printUnknownIdsList, false);

    statsShowed = true;

    // process stats
    var chartsData = statsLine.split("|");

    var chartTotal = chartsData[0];
    var chartSite = chartsData[1].split(";");
    var chartSex = chartsData[2].split(";");
    var chartAge = chartsData[3].split(";");

    // draw charts
    drawChartRatioFounded(chartTotal);
    drawChartSampleTissue(chartSite);
    drawChartAge(chartAge);
    drawGraphSex(chartSex);
}

function drawChartRatioFounded(chartTotal) {

    var values = chartTotal.split(",");

    var chart = document.getElementById("chart-ratio-founded");
    var ctx = chart.getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ["Known IDs", "Unknown IDs"],
            datasets: [{
                label: 'ratio',
                data: values,
                backgroundColor: [
                    window.chartColors.edirexPurple,
                    window.chartColors.grey
                ],
                borderWidth: 2
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Ratio of unavailable data'
            }/*,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }*/
        }
    });
}

function drawGraphSex(dataRaw) {
    var data = extractDataToChart(dataRaw);

    // draw graph
    var ctx = document.getElementById("chart-sex").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: data[0],
            datasets: [{
                label: 'Unavailable data',
                data: data[2],
                backgroundColor:
                    Array(data[0].length).fill(window.chartColors.grey)
            }, {
                label: 'Available data',
                data: data[1],
                backgroundColor:
                    Array(data[0].length).fill(window.chartColors.edirexBlue)
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Filtered by Sex'
            },
            scales: {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

function drawChartAge(dataRaw) {
    var data = extractDataToChart(dataRaw);

    // draw graph
    var ctx = document.getElementById("chart-age").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: data[0],
            datasets: [{
                label: 'Unavailable data',
                data: data[2],
                backgroundColor:
                    Array(data[0].length).fill(window.chartColors.grey)
            }, {
                label: 'Available data',
                data: data[1],
                backgroundColor:
                    Array(data[0].length).fill(window.chartColors.edirexBlue)
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Filtered by Age'
            },
            scales: {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

function extractDataToChart(list) {
    var labels = [];
    var final = [];
    var unknown = [];
    list.forEach(function (entry) {
        var line = entry.split(",");

        labels.push(line[0]);
        final.push(line[1]);
        unknown.push(line[2]);
    });

    return [labels, final, unknown];
}

function drawChartSampleTissue(dataRaw) {
    var data = extractDataToChart(dataRaw);

    // draw graph
    var ctx = document.getElementById("chart-sample-tissue").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: data[0],
            datasets: [{
                label: 'Unavailable data',
                data: data[2],
                backgroundColor:
                    Array(data[0].length).fill(window.chartColors.grey)
            }, {
                label: 'Available data',
                data: data[1],
                backgroundColor:
                    Array(data[0].length).fill(window.chartColors.edirexBlue)
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Filtered by Primary Site'
            },
            scales: {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

/**
 * Show / hide list of found ids
 */
function showHideIds() {
    var list = document.getElementById("unknown-ids-list");

    var btnShow = document.getElementById("unknown-ids-list-btn-show");
    var btnHide = document.getElementById("unknown-ids-list-btn-hide");

    if (list.style.display !== "block") {
        list.style.display = "block";

        btnShow.style.display = "none";
        btnHide.style.display = "block";
    } else {
        list.style.display = "none";

        btnShow.style.display = "block";
        btnHide.style.display = "none";
    }
}

function request(){
    can = true;
    if (window.XMLHttpRequest) {
        // code for modern browsers
        xhr = new XMLHttpRequest();
    } else {
        // code for old IE browsers
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhr = new XMLHttpRequest();

    xhr.onreadystatechange = processRequest;

    var string = serverUrl + "/api?id=";
    string = string + port;

    xhr.open('GET',string, true);
    xhr.send();
    xhr.addEventListener("readystatechange", processRequest, false);

}

function getParameterValue(name)
{
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null ) return "";
    else return results[1];
}



