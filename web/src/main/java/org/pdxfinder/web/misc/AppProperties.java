package org.pdxfinder.web.misc;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "edirex")
public class AppProperties {

    private String serverapidomain;
    private String serverapiport;
    private String datahubapi;
    private String k8sapi;
    private boolean debugmode;

    public String getServerapidomainport() {
        return serverapidomain + ":" + serverapiport;
    }

    public String getServerapidomain() {
        return serverapidomain;
    }

    public String getServerapiport() {
        return serverapiport;
    }

    public String getDatahubapi() {
        return datahubapi;
    }

    public String getK8sapiinstances() {
        return k8sapi;
    }

    public String getK8sapi() {
        return k8sapi + "/api/v1";
    }

    public boolean isDebugmode() {
        return debugmode;
    }

    public void setDatahubapi(String datahubapi) {
        this.datahubapi = datahubapi;
    }

    public void setK8sapi(String k8sapi) {
        this.k8sapi = k8sapi;
    }

    public void setDebugmode(boolean debugmode) {
        this.debugmode = debugmode;
    }

}
