package org.pdxfinder.web.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Radim Sasinka
 * Controller for Type of Access page
 */
@Controller
public class ToAController {

    @RequestMapping("/typeOfAccess")
    public String toa() {
        return "typeOfAccess";
    }

    @RequestMapping("/toa/free")
    public String free(){
        return "toa/free";
    }

    @RequestMapping("/toa/collaborative")
    public String collaborative(){
        return "toa/collaborative";
    }
}
