package org.pdxfinder.web.controllers;

import org.pdxfinder.web.misc.AppProperties;
import org.pdxfinder.web.model.Identity;
import org.pdxfinder.web.model.Login;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@ControllerAdvice
public class AAI {

    private AppProperties appProperties;

    public AAI(AppProperties appProperties) {
        this.appProperties = appProperties;
    }

    /**
     * Add Login to every Controller in this package. To use: just add Login login to method parameters.
     * @param id extract from header X-Attribute-Id sent by proxy
     * @return Login object with isAuthorized method
     */
    @ModelAttribute("login")
    public Login getLogin(@RequestHeader(value = "X-Attribute-Id", required = false) String id){

        String authPass = System.getenv("AUTH_PASS");
        if (authPass == null) authPass = "";

        if (appProperties.isDebugmode() || authPass.equals("1"))
            return Login.getLogin("test");
        return Login.getLogin(id.replace('@', '_'));
    }

    /**
     * Add Identity to every Controller in this package. To use: just add Identity identity to method parameters.
     * @param name extract from header X-Attribute-Name sent by proxy
     * @param email extract from header X-Attribute-Mail sent by proxy
     * @return null if user is not logged in, Identity object otherwise
     */
    @ModelAttribute("identity")
    public Identity getIdentity(@RequestHeader(value = "X-Attribute-Name", required = false) String name,
                                @RequestHeader(value = "X-Attribute-Mail", required = false) String email) {

        String authPass = System.getenv("AUTH_PASS");
        if (authPass == null) authPass = "";

        if (appProperties.isDebugmode() || authPass.equals("1"))
            return new Identity("test", "test@test.test");

        if (name == null || email == null || name.equals("(null)") || email.equals("(null)"))
            return null;

        Charset w1252 = StandardCharsets.ISO_8859_1;
        Charset utf8 = StandardCharsets.UTF_8;

        name = new String(name.getBytes(w1252), utf8);
        email = new String(email.getBytes(w1252), utf8);

        return new Identity(name, email);
    }
}
