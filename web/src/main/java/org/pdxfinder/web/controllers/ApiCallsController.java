package org.pdxfinder.web.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.pdxfinder.web.misc.AppProperties;
import org.pdxfinder.web.model.Instance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Luboslav Pivac
 */
@RestController
public class ApiCallsController {

    private AppProperties appProperties;
    private Logger logger = LoggerFactory.getLogger(SearchController.class);

    public ApiCallsController(AppProperties appProperties) {
        this.appProperties = appProperties;
    }

    @RequestMapping(value = "/instance-status-all", method = RequestMethod.GET)
    ResponseEntity<String> instanceStatusAll(@RequestParam String user) {
        String url = appProperties.getK8sapi() + "/cbioondemands?userId=" + user;
        return sendRequest(url, HttpMethod.GET);
    }

    @RequestMapping(value = "/instance-status", method = RequestMethod.GET)
    ResponseEntity<String> instanceStatus(@RequestParam String user, @RequestParam String id) {
        String url = appProperties.getK8sapi() + "/cbioondemand?user.userId=" + user + "&id=" + id;
        return sendRequest(url, HttpMethod.GET);
    }

    @RequestMapping(value = "/instance-extend", method = RequestMethod.GET)
    ResponseEntity<String> instanceExtend(@RequestParam String user, @RequestParam String id, @RequestParam String extend) {
        String url = appProperties.getK8sapi() + "/cbioondemand";
        String jsonData = String.format("{\"id\":\"%s\",\"user\":{\"userId\":\"%s\"},\"timeToExtend\":%s}", id, user, extend);
        return sendRequest(url, HttpMethod.PUT, jsonData);
    }

    @RequestMapping(value = {"/instance-delete", "/export-cancel"}, method = RequestMethod.GET)
    ResponseEntity<String> exportCancel(@RequestParam String user, @RequestParam String id) {
        String url = appProperties.getK8sapi() + "/cbioondemand?user.userId=" + user + "&id=" + id;
        return sendRequest(url, HttpMethod.DELETE);
    }

    @RequestMapping(value = "/export-status", method = RequestMethod.GET)
    ResponseEntity<String> exportStatus(@RequestParam String user, @RequestParam String id) {
        String url = appProperties.getK8sapi() + "/cbioondemand?user.userId=" + user + "&id=" + id;

        ResponseEntity<String> responseEntity = sendRequest(url, HttpMethod.GET);
        if (responseEntity.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
            return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String instanceStatus = new Gson().fromJson(responseEntity.getBody(), Instance.class).getStatus();
        return new ResponseEntity<>(instanceStatus, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/get-k8s-api-url", method = RequestMethod.GET)
    ResponseEntity<String> getK8sApiUrl() {
        Map<String, String> dictionary = new HashMap<String, String>();
        dictionary.put("url", appProperties.getK8sapiinstances());

        String result = new GsonBuilder().create().toJson(dictionary);
        return new ResponseEntity<>(result, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/api", method = RequestMethod.GET)
    ResponseEntity<String> api(@RequestParam int id) {
        String url = appProperties.getServerapidomainport() + "/api?id=" + id;
        return sendRequest(url, HttpMethod.GET);
    }

    private ResponseEntity<String> sendRequest(String url, HttpMethod httpMethod) {
        return sendRequest(url, httpMethod, "");
    }

    private ResponseEntity<String> sendRequest(String url, HttpMethod httpMethod, String jsonData) {
        try {
            ResponseEntity<String> responseFromAPI = new RestTemplate().
                    exchange(url, httpMethod, getHttpEntity(jsonData), String.class);

            return new ResponseEntity<>(responseFromAPI.getBody(), HttpStatus.ACCEPTED);
        } catch (Exception e) {
            logger.error(String.format("Request failed: %s %s %s", url, httpMethod.toString(), e.getMessage()));
            return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private HttpEntity getHttpEntity(String jsonData) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        if (jsonData == null || jsonData.equals("")) {
            return new HttpEntity(headers);
        } else {
            return new HttpEntity<>(jsonData, headers);
        }
    }
}
