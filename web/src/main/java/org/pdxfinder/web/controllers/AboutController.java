package org.pdxfinder.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Luboslav Pivac
 */
@Controller
public class AboutController {

    @RequestMapping("/about")
    public String about(){
        return "about";
    }

    @RequestMapping("/about/pdx-finder-publication")
    public String publication(){
        return "about/pdx-finder-publication";
    }

    @RequestMapping("/about/terms-of-use")
    public String use(){
        return "about/terms-of-use";
    }

    @RequestMapping("/about/faq")
    public String faq(){
        return "about/faq";
    }

    @RequestMapping("/about/privacy-policy")
    public String policy(){
        return "about/privacy-policy";
    }

    @RequestMapping("/about/pdx-standard")
    public String standard(){
        return "about/pdx-standard";
    }

    @RequestMapping("/about/epdx")
    public String epdx(){return "about/epdx";}

    @RequestMapping("/about/objectives")
    public String objectives(){return "about/objectives";}

}
