package org.pdxfinder.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ProvidersController {

    @RequestMapping(value ="/source/ircc", method = RequestMethod.GET)
    String Ircc(){return "source/ircc";}

    @RequestMapping(value ="/source/trace", method = RequestMethod.GET)
    String Trace(){return "source/trace";}

//    @RequestMapping("/source/epdx")
//    String Epdx(){return "source/epdx";}

}
