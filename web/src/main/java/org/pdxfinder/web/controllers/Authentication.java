package org.pdxfinder.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Authentication {

    @RequestMapping("/authentication")
    public String authentication(Model model) {
        model.addAttribute("returnTo", "/");
        return "authentication";
    }
}
