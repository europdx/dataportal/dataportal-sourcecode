package org.pdxfinder.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Luboslav Pivac
 */
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class ContactController {

    @RequestMapping("/contact")
    public String contact(){
        return "contact";
    }

}
