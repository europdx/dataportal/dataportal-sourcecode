package org.pdxfinder.web.controllers;

import org.pdxfinder.web.model.Identity;
import org.pdxfinder.web.model.Instance;
import org.pdxfinder.web.model.Login;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class MyServicesController {

    @RequestMapping("/my-services")
    public String onlineServices(Login login, Identity identity, Model model){
        if (!login.isAuthorized() || identity == null) {
            model.addAttribute("returnTo", "/my-services");
            return "authentication";
        }

        model.addAttribute("identityId", login.getId());
        model.addAttribute("identityName", identity.getName());
        model.addAttribute("identityEmail", identity.getEmail());
        model.addAttribute("htmlPage", "my-services");

        return "my-services";
    }


    @RequestMapping("/cbioportal")
    public @ResponseBody Instance getInstance(@RequestParam String user, @RequestParam String id){
        return null;
    }

    @RequestMapping("/cbioportals")
    public  @ResponseBody List<Instance> getInstances(){
        return null;
    }

}
