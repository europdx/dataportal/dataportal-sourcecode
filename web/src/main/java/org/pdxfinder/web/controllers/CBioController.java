package org.pdxfinder.web.controllers;

import com.google.gson.Gson;
import org.pdxfinder.services.SearchService;
import org.pdxfinder.services.ds.ModelForQuery;
import org.pdxfinder.services.dto.ExportDTO;
import org.pdxfinder.web.misc.AppProperties;
import org.pdxfinder.web.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Luboslav Pivac
 */
@Controller
public class CBioController {

    private AppProperties appProperties;
    private SearchService searchService;
    private Logger logger = LoggerFactory.getLogger(SearchController.class);

    public CBioController(AppProperties appProperties, SearchService searchService) {
        this.appProperties = appProperties;
        this.searchService = searchService;
    }

    @RequestMapping("/search/cbio2")
    ModelAndView cbio2(HttpServletResponse response, ModelMap model, RedirectAttributes attributes,
                       @RequestParam("query") Optional<String> query,
                       @RequestParam("datasource") Optional<List<String>> datasource,
                       @RequestParam("diagnosis") Optional<List<String>> diagnosis,
                       @RequestParam("patient_age") Optional<List<String>> patient_age,
                       @RequestParam("patient_treatment") Optional<List<String>> patient_treatment,
                       @RequestParam("patient_treatment_status") Optional<List<String>> patient_treatment_status,
                       @RequestParam("patient_gender") Optional<List<String>> patient_gender,
                       @RequestParam("sample_origin_tissue") Optional<List<String>> sample_origin_tissue,
                       @RequestParam("cancer_system") Optional<List<String>> cancer_system,
                       @RequestParam("sample_tumor_type") Optional<List<String>> sample_tumor_type,
                       @RequestParam("mutation") Optional<List<String>> mutation,
                       @RequestParam("drug") Optional<List<String>> drug,
                       @RequestParam("project") Optional<List<String>> project,
                       @RequestParam("data_available") Optional<List<String>> data_available,
                       @RequestParam("breast_cancer_markers") Optional<List<String>> breast_cancer_markers,
                       @RequestParam("access_modalities") Optional<List<String>> access_modalities,
                       @RequestParam("copy_number_alteration") Optional<List<String>> copy_number_alteration,
                       @RequestParam("gene_expression") Optional<List<String>> gene_expression,
                       @RequestParam("cytogenetics") Optional<List<String>> cytogenetics,
                       @RequestParam("model_id") Optional<List<String>> model_id,
                       Login login
    ) {

        logger.info("query" + query.toString());
        logger.info("datasource" + datasource.toString());
        logger.info("diagnosis" + diagnosis.toString());
        logger.info("patient_age" + patient_age.toString());
        logger.info("patient_treatment" + patient_treatment.toString());
        logger.info("patient_treatment_status" + patient_treatment_status.toString());
        logger.info("patient_gender" + patient_gender.toString());
        logger.info("sample_origin_tissue" + sample_origin_tissue.toString());
        logger.info("cancer_system" + cancer_system.toString());
        logger.info("sample_tumor_type" + sample_tumor_type.toString());
        logger.info("mutation" + mutation.toString());
        logger.info("drug" + drug.toString());
        logger.info("project" + project.toString());
        logger.info("data_available" + data_available.toString());
        logger.info("breast_cancer_markers" + breast_cancer_markers.toString());
        logger.info("access_modalities" + access_modalities.toString());
        logger.info("copy_number_alteration" + copy_number_alteration.toString());
        logger.info("gene_expression" + gene_expression.toString());
        logger.info("cytogenetics" + cytogenetics.toString());
        logger.info("model_id" + cytogenetics.toString());


        List<String> ids = captureIDs(query, datasource, diagnosis, patient_age, patient_treatment, patient_treatment_status,
                patient_gender, sample_origin_tissue, cancer_system, sample_tumor_type, mutation, drug, project,
                data_available, breast_cancer_markers, access_modalities, copy_number_alteration, gene_expression, cytogenetics, model_id);

        String tmp_list = createTmpList(ids);

        if(tmp_list == null) {
            model.addAttribute("retryHide", "true");
            return new ModelAndView("unavailable-k8s");
        }

        return TryToCreateInstance(model, attributes, login.getId(), tmp_list);
    }

    @RequestMapping("/search/retry")
    ModelAndView retry(ModelMap model, RedirectAttributes attributes, Login login, @RequestParam("tmplist") String tmpList) {
        return TryToCreateInstance(model, attributes, login.getId(), tmpList);
    }

    @RequestMapping("/cbiostart2")
    String cbiostart2(Model model, 
                      @PathParam("href") String href, 
                      @PathParam("user") String user, 
                      @PathParam("id") String id, 
                      @PathParam("stats") String stats, 
                      Identity identity) {

        try {
            id =  URLDecoder.decode(id,"UTF-8");
            href = URLDecoder.decode(href,"UTF-8");
            user = URLDecoder.decode(user, "UTF-8");
            stats = URLDecoder.decode(stats, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        model.addAttribute("link", href);
        model.addAttribute("id", id);
        model.addAttribute("user", user);

        // parse stats
        List<StatsStudy> statsStudies = new ArrayList<>();

        if (!stats.equals(""))
            statsStudies = Arrays.asList(new Gson().fromJson(stats, StatsStudy[].class));

        model.addAttribute("stats", statsStudies);
        model.addAttribute("identityName", identity.getName());
        model.addAttribute("htmlPage", "search");

        return "cbio-k8s";
    }

    private ModelAndView TryToCreateInstance(ModelMap model, RedirectAttributes attributes, String loginId, String tmpList) {
        // request to k8s api to create new instance
        Instance instance = createInstance(new User(loginId), tmpList);
        if (instance == null) {
            logger.error("creating instance failed");
            model.addAttribute("retryLink", "/data/search/retry?tmplist=" + tmpList);
            return new ModelAndView("unavailable-k8s");
        }

        // get stats
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;charset=UTF-8");
        HttpEntity httpEntity = new HttpEntity(headers);
        String stats; // TODO
        try {
            stats = new RestTemplate().exchange(appProperties.getDatahubapi() + "/tmplists/" + tmpList + "/statistics", HttpMethod.GET, httpEntity, String.class).getBody();
        }
        catch (Exception e) {
            logger.error("request to datahub api failed");
            model.addAttribute("retryLink", "/data/search/retry?tmplist=" + tmpList);
            return new ModelAndView("unavailable-k8s");
        }

        attributes.addAttribute("user", loginId);
        attributes.addAttribute("id", instance.getId());
        attributes.addAttribute("href", instance.getUrl());
        attributes.addAttribute("stats", stats);

        return new ModelAndView("redirect:/cbiostart2");
    }

    private String createTmpList(List<String> ids) {
        class Payload {
            private List<String> pdxmodel_list;

            private Payload(List<String> pdxmodel_list) {
                this.pdxmodel_list = pdxmodel_list;
            }

            public List<String> getPdxmodel_list() {
                return pdxmodel_list;
            }

            public void setPdxmodel_list(List<String> pdxmodel_list) {
                this.pdxmodel_list = pdxmodel_list;
            }
        }

        RestTemplate restTemplate = new RestTemplate();

        Payload payload = new Payload(ids);

        ResponseEntity<TmpList> response;
        try {
            response = restTemplate.postForEntity(appProperties.getDatahubapi() + "/tmplists", payload, TmpList.class);
        } catch (Exception e) {
            logger.error("POST request to datahub api to create tmplist failed");
            return null;
        }

        if (response.getStatusCode() != HttpStatus.CREATED){
            logger.error("empty tmp_list on datahub");
            return null;
        }

        return response.getBody().getTmplistid();
    }

    private Instance createInstance(User user, String tmp_list) {
        RestTemplate restTemplate = new RestTemplate();
        InitialBody2 request = new InitialBody2(user, tmp_list);

        ResponseEntity<Instance> response;
        try {
             response = restTemplate.postForEntity(appProperties.getK8sapi() + "/cbioondemand", request, Instance.class);
        } catch (Exception e) {
            logger.error("resuest to k8s api to create instance failed - " + e.getMessage());
            return null;
        }

        if (response.getStatusCode() != HttpStatus.OK){
            logger.error("Response status code: " + response.getStatusCode());
            return null;
        }
        return response.getBody();
    }

    private List<String> captureIDs(Optional<String> query,
                                    Optional<List<String>> datasource,
                                    Optional<List<String>> diagnosis,
                                    Optional<List<String>> patient_age,
                                    Optional<List<String>> patient_treatment,
                                    Optional<List<String>> patient_treatment_status,
                                    Optional<List<String>> patient_gender,
                                    Optional<List<String>> sample_origin_tissue,
                                    Optional<List<String>> cancer_system,
                                    Optional<List<String>> sample_tumor_type,
                                    Optional<List<String>> mutation,
                                    Optional<List<String>> drug,
                                    Optional<List<String>> project,
                                    Optional<List<String>> data_available,
                                    Optional<List<String>> breast_cancer_markers,
                                    Optional<List<String>> access_modalities,
                                    Optional<List<String>> copy_number_alteration,
                                    Optional<List<String>> gene_expressions,
                                    Optional<List<String>> cytogenetics,
                                    Optional<List<String>> model_id) {

        /* DS: old
        ExportDTO eDTO = searchService.export(query, datasource,
                diagnosis, patient_age, patient_treatment, patient_treatment_status, patient_gender, sample_origin_tissue, cancer_system,
                sample_tumor_type, mutation, drug, project, data_available, breast_cancer_markers, access_modalities, copy_number_alteration);
         */

        /* DS: to be decommnted
        ExportDTO eDTO = searchService.export(query, datasource,
                diagnosis, patient_age, patient_treatment, patient_treatment_status, patient_gender, sample_origin_tissue, cancer_system,
                sample_tumor_type, mutation, drug, project, data_available, breast_cancer_markers, copy_number_alteration,
                gene_expression, cytogenetics);
         */

        // DS: temp - remove
        ExportDTO eDTO = searchService.export(query, datasource,
                diagnosis, patient_age, patient_treatment, patient_treatment_status, patient_gender, sample_origin_tissue, cancer_system,
                sample_tumor_type, mutation, drug, project, data_available, breast_cancer_markers, access_modalities,
                copy_number_alteration, gene_expressions, cytogenetics, model_id);

        return eDTO.getResults().stream().map(ModelForQuery::getExternalId).collect(Collectors.toList());
    }
}
