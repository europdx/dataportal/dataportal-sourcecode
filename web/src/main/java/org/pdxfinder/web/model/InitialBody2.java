package org.pdxfinder.web.model;

public class InitialBody2 {
    private User user;
    private String sampleIDs;

    public InitialBody2() {
    }

    public InitialBody2(User user, String sampleIDs) {
        this.user = user;
        this.sampleIDs = sampleIDs;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSampleIDs() {
        return sampleIDs;
    }

    public void setSampleIDs(String sampleIDs) {
        this.sampleIDs = sampleIDs;
    }
}
