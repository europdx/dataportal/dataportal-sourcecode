package org.pdxfinder.web.model;

public class StatsStudy {
    private String study;
    private String samples;
    private StatsMolecular[] cnas;
    private StatsMolecular[] mutations;
    private StatsMolecular[] expressions;

    public String getStudy() {
        return study;
    }

    public String getSamples() {
        return samples;
    }

    public StatsMolecular[] getCnas() {
        return cnas;
    }

    public StatsMolecular[] getMutations() {
        return mutations;
    }

    public StatsMolecular[] getExpressions() {
        return expressions;
    }

    public void setStudy(String study) {
        this.study = study;
    }

    public void setSamples(String samples) {
        this.samples = samples;
    }

    public void setCnas(StatsMolecular[] cnas) {
        this.cnas = cnas;
    }

    public void setMutations(StatsMolecular[] mutations) {
        this.mutations = mutations;
    }

    public void setExpressions(StatsMolecular[] expressions) {
        this.expressions = expressions;
    }

    public int getCnasCount() {
        if (cnas == null) {
            return 0;
        }

        int result = 0;
        for (StatsMolecular cna : cnas)
            result += cna.getCount();

        return result;
    }

    public int getMutationsCount() {
        if (mutations == null) {
            return 0;
        }

        int result = 0;
        for (StatsMolecular mut : mutations)
            result += mut.getCount();
        return result;
    }

    public int getExpressionsCount() {
        if (expressions == null) {
            return 0;
        }

        int result = 0;
        if (expressions.length > 0)
            result = expressions[0].getCount();
        return result;
    }
}
