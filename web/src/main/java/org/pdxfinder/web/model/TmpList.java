package org.pdxfinder.web.model;

public class TmpList {
    private String tmplistid;

    public TmpList() {
    }

    public TmpList(String tmplistid) {
        this.tmplistid = tmplistid;
    }

    public String getTmplistid() {
        return tmplistid;
    }

    public void setTmplistid(String tmplistid) {
        this.tmplistid = tmplistid;
    }
}
