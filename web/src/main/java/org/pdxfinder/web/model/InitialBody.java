package org.pdxfinder.web.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Luboslav Pivac
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InitialBody {

    private String ids;

    public String getIds() {
        return this.ids;
    }


    public void setId(String ids) {
        this.ids = ids;
    }

    @Override
    public String toString() {
        return "Ids ={" +
                ids +
                '}';
    }

}
