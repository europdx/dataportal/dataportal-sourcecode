package org.pdxfinder.web.model;

public class Login {
    private boolean authorized;
    private String id;

    public Login() {
    }

    public Login(boolean authorized, String id) {
        this.authorized = authorized;
        this.id = id;
    }

    static public Login getLogin(String name){
        if(name != null && !name.equals("(null)"))
            return new Login(true, name);
        return  new Login(false, null);
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Login{" +
                "authorized=" + authorized +
                ", id='" + id + '\'' +
                '}';
    }
}
