package org.pdxfinder.web.model;

public class StatsMolecular {
    private int count;
    private String stable_id;

    public int getCount() {
        return count;
    }

    public String getStable_id() {
        return stable_id;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setStable_id(String stable_id) {
        this.stable_id = stable_id;
    }
}
