package org.pdxfinder.web.model;


public class Instance {
    private String id;
    private User user;
    private long secondsToExpire;
    private String url;
    private String status;

    public Instance() {
    }

    public Instance(String id, User user, long secondsToExpire, String url, String status) {
        this.id = id;
        this.user = user;
        this.secondsToExpire = secondsToExpire;
        this.url = url;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getSecondsToExpire() {
        return secondsToExpire;
    }

    public void setSecondsToExpire(long secondsToExpire) {
        this.secondsToExpire = secondsToExpire;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
