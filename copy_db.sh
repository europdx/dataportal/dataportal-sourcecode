#!/bin/bash -v

rm ~/Documents/graph.tgz
cd ~/Documents/pdx.graphdb
tar -czvf ../graph.tgz *

cd ~/Documents

ssh dataportal-beta rm /home/pdxuser/dataportal-docker/neo4j-data/graph.tgz
scp ~/Documents/graph.tgz dataportal-beta:/home/pdxuser/dataportal-docker/neo4j-data/graph.tgz
ssh -t dataportal-beta 'cd /home/pdxuser/dataportal-docker/; ./reload_db.sh'
