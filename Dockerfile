FROM maven:3.6.0-jdk-8-alpine AS build
WORKDIR /app
COPY . /app
RUN cd /app && \
    mvn clean package -DskipTests && \
    mv web/target/web-1.0.0.jar /app/  && \
    mv indexer/target/indexer-1.0.0.jar /app/


FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=build /app/web-1.0.0.jar /app
COPY --from=build /app/indexer-1.0.0.jar /app
EXPOSE 8080
CMD ["java","-jar","web-1.0.0.jar"]

