#!/bin/bash -v

rm -r ~/Documents/pdx.graphdb
cp -r ~/Documents/new-release-seeddb/pdx.graphdb ~/Documents

java -jar indexer/target/indexer-1.0.0.jar load --group=EurOPDX --data-dir=/home/dalibor/ebi-data/200711-new-release/europdx

#rm ~/Documents/graph.tgz
#cd ~/Documents/pdx.graphdb
#tar -czvf ../graph.tgz *
#cd ~/Documents

#ssh dataportal-beta rm /home/pdxuser/dataportal-docker/neo4j-data/graph.tgz
#scp ~/Documents/graph.tgz dataportal-beta:/home/pdxuser/dataportal-docker/neo4j-data/graph.tgz
#ssh -t dataportal-beta 'cd /home/pdxuser/dataportal-docker/; ./reload_db.sh'
